extends Area2D

const LOOT_NAME := "Loot"
const Player = preload("res://scenes/characters/player.gd")
onready var _sprite := $Sprite as Sprite


func _on_Loot_body_entered(player: Player) -> void:
	player.collect_loot()
	get_tree().call_group("loot display", "collect_loot", _sprite.texture)
	queue_free()
