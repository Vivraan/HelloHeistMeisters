extends Popup

signal entered_correct_pin
export(AudioStream) var _button_click_audio: AudioStream = \
		preload("res://audio/twoTone1.ogg")
export(AudioStream) var _correct_pin_audio: AudioStream = \
		preload("res://audio/threeTone1.ogg")
export(StreamTexture) var _indicator_green := \
		preload("res://assets/interface/png/dotGreen.png")
export(StreamTexture) var _indicator_red := \
		preload("res://assets/interface/png/dotRed.png")
export(StreamTexture) var _indicator_yellow := \
		preload("res://assets/interface/png/dotYellow.png")
var pin := []
var _guess: Array
var _allow_interaction := false
onready var _display := $VBoxContainer/Display/Label as Label
onready var _indicator := \
		$VBoxContainer/Buttons/GridContainer/StatusIndicator as TextureRect
onready var _audio := $AudioStreamPlayer as AudioStreamPlayer
onready var _delayed_call := $Timer as Timer


func _ready() -> void:
	_reset_lock()

	for child in $VBoxContainer/Buttons/GridContainer.get_children():
		if child is Button:
			var button: Button = child
# warning-ignore:return_value_discarded
			button.connect("pressed", self,
					"_on_ok_button_pressed"
						if button.name == "ButtonOK"
						else "_on_num_button_pressed",
					 [button.text])


func _input(event: InputEvent) -> void:
	if event is InputEventKey and event.is_pressed() and _allow_interaction:
		var key_event: InputEventKey = event
		var num: int
		if KEY_0 <= key_event.scancode and key_event.scancode <= KEY_9:
			num = key_event.scancode - KEY_0
			_enter_digit(num)
		elif KEY_KP_0 <= key_event.scancode and key_event.scancode <= KEY_KP_9:
			num = key_event.scancode - KEY_KP_0
			_enter_digit(num)
		elif key_event.scancode == KEY_ENTER or key_event.scancode == KEY_KP_ENTER:
			_check_guess()
		else:
			# do nothing, that's the wrong key!
			return


func _on_num_button_pressed(button_text: String) -> void:
	_enter_digit(int(button_text))


func _on_ok_button_pressed(_button_text: String) -> void:
	_check_guess()


func _on_lock_reset_once() -> void:
	_reset_lock()


func _on_unlock_once() -> void:
	emit_signal("entered_correct_pin")


func _on_Numpad_about_to_show() -> void:
	if not _display.text.empty():
		_check_guess()
	_allow_interaction = true


func _on_Numpad_popup_hide() -> void:
	_allow_interaction = false


func _enter_digit(button_value: int) -> void:
	_audio.stream = _button_click_audio
	_audio.play()
	_guess.append(button_value)
	_update_display()


func _update_display() -> void:
	_display.text = PoolStringArray(_guess).join("")
	if len(_guess) == len(pin):
		_indicator.texture = _indicator_yellow
		_check_guess()


func _check_guess() -> void:
	if _guess == pin:
		_indicator.texture = _indicator_green

		_audio.stream = _correct_pin_audio
		_audio.play()

# warning-ignore:return_value_discarded
		_delayed_call.connect(
				"timeout", self, "_on_unlock_once", [], CONNECT_ONESHOT)
		_delayed_call.start()
	else:
# warning-ignore:return_value_discarded
		_delayed_call.connect(
				"timeout", self, "_on_lock_reset_once", [], CONNECT_ONESHOT)
		_delayed_call.start()


func _reset_lock() -> void:
	_indicator.texture = _indicator_red
	_display.text = ""
	_guess.clear()
