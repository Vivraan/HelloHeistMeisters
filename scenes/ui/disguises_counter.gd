extends ItemList

export(StreamTexture) var _box_texture := \
		preload("res://assets/sprites/tiles/tile_156.png")


func update_counter(count: int) -> void:
	clear()
	for __ in range(count):
		add_icon_item(_box_texture, false)
