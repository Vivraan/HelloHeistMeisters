extends NinePatchRect

onready var _item_list := $LootCounter as ItemList


func _ready() -> void:
	hide()


func collect_loot(texture: StreamTexture) -> void:
	show()
	_item_list.add_icon_item(texture, false)
