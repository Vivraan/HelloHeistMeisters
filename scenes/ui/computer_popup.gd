extends Popup

export var _default_text := """
CAUTION
=======
Forgetting access codes can lead to termination of your Right to Life contract!

Current combination: %s.

Be sure to write it and keep it on your person at all times!

GLORY TO ORVH!!!1!
"""
onready var _label := $NinePatchRect/CenterContainer/NinePatchRect/Label as Label


func set_text(_pin: Array):
	_label.text = _default_text % (PoolStringArray(_pin).join(""))
