extends TextureProgress

const DEFEAT_SCENE_NAME := "res://scenes/levels/defeat.tscn"
export(int, 5, 20) var suspicion_multiplier := 10


func _ready() -> void:
# warning-ignore:return_value_discarded
	DebugOverlay.add_monitor("Suspicion (%)", self, ".:value")
	value = 0;


func _process(_delta: float) -> void:
	value -= step


func player_seen() -> void:
	value += step * suspicion_multiplier

	if value == max_value:
		_end_game()


func _end_game() -> void:
# warning-ignore:return_value_discarded
	get_tree().change_scene(DEFEAT_SCENE_NAME)
