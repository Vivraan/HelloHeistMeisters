extends Node2D

signal pin_overriden
const Character: Script = preload("res://scenes/characters/character.gd")
const ComputerPopup: Script = preload("res://scenes/ui/computer_popup.gd")
export(int, 4, 8) var _pin_length := 4
export var _lockables_group := "DefaultLockable"
var _allow_interaction := false
var _pin: Array
var _label: Label
onready var _computer_popup := $CanvasLayer/ComputerPopup as ComputerPopup
onready var _light := $Light2D as Light2D
onready var _player = $"/root".find_node("Player", true, false) as Character


func _ready() -> void:
	_pin = HHM_Core.generate_pin(_pin_length)
	_computer_popup.set_text(_pin)
	_label = $Label as Label
	_label.rect_rotation = -rotation_degrees
	_label.text = _lockables_group
	emit_signal("pin_overriden", _pin, _lockables_group)


func _on_Area2D_body_entered(body: Character) -> void:
	if not _player.is_connected("disguise_toggled", self, "_on_disguise_toggled"):
		_player.connect(
			"disguise_toggled", self, "_on_disguise_toggled")
	if body.collision_layer == 1 << HHM_Core.PhysicsLayers2D.PLAYER:
		_allow_interaction = true


func _on_Area2D_body_exited(_body: Character) -> void:
	if _player.is_connected("disguise_toggled", self, "_on_disguise_toggled"):
		_player.disconnect(
			"disguise_toggled", self, "_on_disguise_toggled")
	_allow_interaction = false
	_computer_popup.hide()
	_light.enabled = false


func _on_disguise_toggled(disguised: bool) -> void:
	_allow_interaction = not disguised


func _on_Area2D_input_event(_viewport: Node, event: InputEvent, _shape_idx: int) -> void:
	if event.is_action_pressed("interact") and _allow_interaction:
		_computer_popup.popup_centered()
		_light.enabled = true
