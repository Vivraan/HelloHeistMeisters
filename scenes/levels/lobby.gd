extends Control

export var first_scene_name := "res://scenes/levels/level_1.tscn"
export var tutorial_scene_name := "res://scenes/levels/level_tutorial.tscn"
onready var _play_button := \
		$CenterContainer/Background/VBoxContainer/CenterContainer/Options/PlayButton \
		as Button
onready var _quit_button := \
		$CenterContainer/Background/VBoxContainer/CenterContainer/Options/QuitButton \
		as Button


func _on_PlayButton_pressed() -> void:
	get_tree().change_scene(first_scene_name)


func _on_QuitButton_pressed() -> void:
	get_tree().quit()


func _on_TutorialButton_pressed() -> void:
	get_tree().change_scene(tutorial_scene_name)
