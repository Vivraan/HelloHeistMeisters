extends CanvasModulate

export var _dark_color := Color(0.17, 0.17, 0.17)
export var _nightvision_color := Color(0.21, 0.74, 0.38)
export var _nightvision_on_audio: AudioStream = \
 		preload("res://audio/nightvision.wav")
export var _nightvision_off_audio: AudioStream = \
		preload("res://audio/nightvision_off.wav")
var _cooling_down := false
onready var _audio := $AudioStreamPlayer as AudioStreamPlayer
onready var _timer := $Timer as Timer


func _ready() -> void:
	visible = true
	color = _dark_color
# warning-ignore:return_value_discarded
	DebugOverlay.add_monitor(
			"Nightvision ready to use", self, "", "_is_ready_to_toggle")
# warning-ignore:return_value_discarded
	DebugOverlay.add_monitor(
			"Nightvision cooldown time left (s)", self, "Timer:time_left")


func toggle_vision_mode() -> void:
	if not _cooling_down:
		if color == _dark_color:
			nightvision_mode()
			_play_audio(_nightvision_on_audio)
		else:
			dark_mode()
			_play_audio(_nightvision_off_audio)
		_cooling_down = true
		_timer.start()


func dark_mode() -> void:
	color = _dark_color
	get_tree().call_group("lights", "show")
	get_tree().call_group("labels", "hide")


func nightvision_mode() -> void:
	color = _nightvision_color
	get_tree().call_group("lights", "hide")
	get_tree().call_group("labels", "show")


func _is_ready_to_toggle() -> bool:
	return not _cooling_down


func _on_Timer_timeout() -> void:
	_cooling_down = false


func _play_audio(audio_to_play: AudioStream) -> void:
	_audio.stream = audio_to_play
	_audio.play()
