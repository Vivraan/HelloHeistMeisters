extends ColorRect

const Loot: Script = preload("res://scenes/loot/loot.gd")
const Player: Script = preload("res://scenes/characters/player.gd")
const VICTORY_SCENE_NAME := "res://scenes/levels/victory.tscn"


func _on_Area2D_body_entered(player: Player) -> void:
	if player.has_node(Loot.LOOT_NAME):
# warning-ignore:return_value_discarded
		get_tree().change_scene(VICTORY_SCENE_NAME)
