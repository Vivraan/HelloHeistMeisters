extends Node2D

const Player: Script = preload("res://scenes/characters/player.gd")
const Message: Script = preload("res://scenes/ui/objective_message_template.gd")
export (float, 10000, 250000, 50) var _sq_transition_speed := 156250.0

func _ready() -> void:
	point_at(0)


func point_at(objective_rank: int) -> void:
	var pointer := $Objectives/Pointer as Sprite
	var place := $Objectives/Markers.get_child(objective_rank) as Position2D
	var message := ($Objectives/Messages.get_child(objective_rank) as Message).message
	# computes a duration which is half as fast for twice as long
	var duration := \
		pointer.position.distance_squared_to(place.position) / _sq_transition_speed
	var tween := $Tween as Tween

# warning-ignore:return_value_discarded
	tween.interpolate_property(
		pointer, "position", pointer.position, place.position,
		duration, Tween.TRANS_SINE, Tween.EASE_IN_OUT)
# warning-ignore:return_value_discarded
	tween.start()
	($MessageAudio as AudioStreamPlayer).play()
	($TutorialGUI/AnimationPlayer as AnimationPlayer).play("MessageChange")
	($TutorialGUI/Control/NinePatchRect/Label as Label).text = message


func _on_MoveArea_body_entered(_body: Player) -> void:
	point_at(1)


func _on_DoorArea_body_entered(_body: Player) -> void:
	point_at(2)


func _on_NVArea_body_entered(_body: Player) -> void:
	get_tree().call_group("vision mode handler", "dark_mode")
	point_at(3)


func _on_BriefcaseArea_body_entered(_body: Player) -> void:
	point_at(4)
