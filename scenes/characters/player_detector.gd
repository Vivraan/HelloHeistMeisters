extends "res://scenes/characters/character.gd"

const Character: Script = preload("res://scenes/characters/character.gd")
export(float, 0.0, 1000.0) var _los_range_pixels := 600.0
export(float, 1.0, 180.0) var _fov_degrees := 40.0
export(Color, RGB) var _detected_color := Color.darkred
export(Color, RGB) var _regular_color := Color.aliceblue
export(float, 0.0, 1.0) var _color_switch_rate := 0.1
export(Array, NodePath) var _excluded_nodes_paths: Array
var _exclude := []
onready var _player = $"/root".find_node("Player", true, false) as Character
onready var _torch = $Torch as Light2D


func _ready() -> void:
	for path in _excluded_nodes_paths:
		_exclude.append(get_node(path))
	_exclude.append(self)


func _process(_delta: float) -> void:
	if _is_player_in_fov() and _is_player_in_los():
		_torch.color = lerp(_torch.color, _detected_color, _color_switch_rate)
		get_tree().call_group("suspicion meter", "player_seen")
	else:
		_torch.color = lerp(_torch.color, _regular_color, _color_switch_rate)
	return


func _is_player_in_fov() -> bool:
	var forward := Vector2.RIGHT.rotated(global_rotation)
	var direction_to_player: Vector2 = (
			_player.position - global_position).normalized()
	var abs_angle := abs(direction_to_player.angle_to(forward))
	return abs_angle <= deg2rad(_fov_degrees * 0.5)


func _is_player_in_los() -> bool:
	var space := get_world_2d().direct_space_state
	var los_obstacle := space.intersect_ray(
			global_position, _player.global_position, _exclude, collision_mask)

	if not los_obstacle:
		return false

	var distance_sq: float = \
			_player.global_position.distance_squared_to(global_position)
	var within_range := distance_sq <= (_los_range_pixels * _los_range_pixels)
	return los_obstacle.collider == _player and within_range
