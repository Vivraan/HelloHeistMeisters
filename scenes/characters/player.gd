extends "res://scenes/characters/character.gd"

signal disguise_toggled
#const TorchScene: PackedScene = preload("res://lightmaps/Torch.tscn")
export(float, 0.0, 1.0) var _disguise_slowdown := 0.25
export var _disguise_max_count := 3
export(int, 3, 30) var _disguise_timeout := 10
export(StreamTexture) var _player_texture := \
		preload("res://assets/sprites/hitman_1/hitman1_stand.png")
export(StreamTexture) var _box_texture := \
		preload("res://assets/sprites/tiles/tile_130.png")
export(Shape2D) var _player_collision := \
		preload("res://scenes/characters/character_undisguised_collision.tres") \
		as Shape2D
export(Shape2D) var _box_collision := \
		preload("res://scenes/characters/character_box_collision.tres") \
		as Shape2D
export(OccluderPolygon2D) var _undisguised_occluder_poly := \
		preload("res://scenes/characters/character_undisguised_occluder.tres")
export(OccluderPolygon2D) var _box_occluder_poly := \
		preload("res://scenes/characters/character_box_occluder.tres")
var _linear_velocity: Vector2
var _velocity_multiplier := 1.0
var _disguised = false
var _disguise_count := 0 setget _set_disguise_count
onready var _sprite := $Sprite as Sprite
onready var _collision := $CollisionShape2D as CollisionShape2D
onready var _occluder = $LightOccluder2D as LightOccluder2D
onready var _highlight = $Light2D as Light2D
onready var _timer := $Timer as Timer
onready var _disguise_label := $DisguiseLabel as Label
#var _torch: Light2D

func _ready() -> void:
	self._disguise_count = _disguise_max_count
	_reveal()
	DebugOverlay.visible = true
# warning-ignore:return_value_discarded
	DebugOverlay.add_monitor("Position (px)", self, ".:global_position")
# warning-ignore:return_value_discarded
	DebugOverlay.add_monitor("Velocity (px/frame)", self, ".:_linear_velocity")
# warning-ignore:return_value_discarded
	DebugOverlay.add_monitor(
			"Look Rotation (°, right is 0°)", self, ".:rotation_degrees")
# warning-ignore:return_value_discarded
	DebugOverlay.add_monitor("Disguises left", self, ".:_disguise_count")
# warning-ignore:return_value_discarded
	DebugOverlay.add_monitor("Disguise wears off in (s)", self, "Timer:time_left")
	_timer.wait_time = _disguise_timeout
	return


# Need a torch? Make one yourself.
#func _enter_tree() -> void:
#	_torch = TorchScene.instance()
#
#
#func _exit_tree() -> void:
#	_torch.queue_free()
#
#
#func _input(event: InputEvent) -> void:
#	if event.is_action_pressed("toggle_torch"):
#		_torch.enabled = not _torch.enabled


func _input(event: InputEvent) -> void:
	if event.is_action_pressed("toggle_vision_mode"):
		get_tree().call_group("vision mode handler", "toggle_vision_mode")
	if event.is_action_pressed("toggle_disguise"):
		_toggle_disguise()
	if event is InputEventKey and event.is_pressed():
		DebugOverlay.visible = not DebugOverlay.visible


func _physics_process(_delta: float) -> void:
	_update_movement()
# warning-ignore:return_value_discarded
	move_and_slide(_linear_velocity * _velocity_multiplier)

	if _disguised:
		_disguise_label.rect_rotation = -rotation_degrees
		_disguise_label.text = str(_timer.time_left).pad_decimals(2)


func _set_disguise_count(value: int) -> void:
	_disguise_count = value
	get_tree().call_group("disguise display", "update_counter", _disguise_count)


func _toggle_disguise() -> void:
	if not _disguised and _disguise_count > 0:
		_disguise()
	else:
		_reveal()
	emit_signal("disguise_toggled", _disguised)


func _disguise() -> void:
	_velocity_multiplier = _disguise_slowdown
	_sprite.texture = _box_texture
	_highlight.texture = _box_texture
	_collision.shape = _box_collision
	_occluder.occluder = _box_occluder_poly
	_disguise_label.show()
	collision_layer = 1 << HHM_Core.PhysicsLayers2D.BOX
	_disguised = true
	self._disguise_count -= 1
	_timer.start()


func _reveal() -> void:
	_velocity_multiplier = 1.0
	_sprite.texture = _player_texture
	_highlight.texture = _player_texture
	_collision.shape = _player_collision
	_occluder.occluder = _undisguised_occluder_poly
	_disguise_label.hide()
	collision_layer = 1 << HHM_Core.PhysicsLayers2D.PLAYER
	_disguised = false


func _update_movement() -> void:
	look_at(get_global_mouse_position())
	var up := Input.is_action_pressed("move_up")
	var down := Input.is_action_pressed("move_down")
	var left := Input.is_action_pressed("move_left")
	var right := Input.is_action_pressed("move_right")

	if up and not down:
		_linear_velocity.y = clamp(_linear_velocity.y - _speed, -_max_speed, 0.0)
	elif down and not up:
		_linear_velocity.y = clamp(_linear_velocity.y + _speed, 0.0, _max_speed)
	else:
		_linear_velocity.y = lerp(_linear_velocity.y, 0.0, _stopping_delay)

	if left and not right:
		_linear_velocity.x = clamp(_linear_velocity.x - _speed, -_max_speed, 0.0)
	elif right and not left:
		_linear_velocity.x = clamp(_linear_velocity.x + _speed, 0.0, _max_speed)
	else:
		_linear_velocity.x = lerp(_linear_velocity.x, 0.0, _stopping_delay)


func collect_loot() -> void:
	var loot := Node.new()
	loot.set_name("Loot")
	add_child(loot)
