extends KinematicBody2D
# Base character from which every other character script derives.

# warning-ignore:unused_class_variable
export(float, 5.0, 200.0) var _speed := 20.0
# warning-ignore:unused_class_variable
export(float, 5.0, 200.0) var _max_speed := 200.0
# warning-ignore:unused_class_variable
export(float, 0.0, 1.0) var _stopping_delay := 0.1



