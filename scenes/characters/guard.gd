extends "res://scenes/characters/player_detector.gd"

export(float, 1.0, 10.0) var _stopping_distance_pixels := 5.0
export(float, 0.0, 1.0) var walk_speed_fraction := 0.5
var _linear_velocity: Vector2
var _possible_destinations: Array
var _waypoints: Array
onready var _navigation := \
		$"/root".find_node("Navigation2D", true, false) as Navigation2D
onready var _destinations := _navigation.get_node("Destinations")
onready var _timer := $Timer as Timer


func _ready() -> void:
	randomize()
	_possible_destinations = _destinations.get_children()
	_reset_path()


func _physics_process(_delta: float) -> void:
	_navigate()


func _on_Timer_timeout() -> void:
	_reset_path()


func _reset_path() -> void:
	var random_point: Vector2 = \
			_possible_destinations[randi() % len(_possible_destinations) - 1]\
			.position
	_waypoints = _navigation.get_simple_path(position, random_point, false)


func _navigate() -> void:
	look_at(_waypoints[0])
	var _sq_distance := position.distance_squared_to(_waypoints[0])
	if _sq_distance > (_stopping_distance_pixels * _stopping_distance_pixels):
		_move()
	else:
		_update_path()


func _move() -> void:
	_linear_velocity = (_waypoints[0] - position).normalized() \
			* _max_speed * walk_speed_fraction

	if is_on_wall():
		_reset_path()

# warning-ignore:return_value_discarded
	move_and_slide(_linear_velocity)


func _update_path() -> void:
	if len(_waypoints) == 1:
		if _timer.is_stopped():
			_timer.start()
	else:
		_waypoints.pop_front()
