class_name HHM_Core
extends Object

enum PhysicsLayers2D \
{
	PLAYER,
	TILE_MAP,
	NPC,
	COMPUTER,
	BOX,
}


static func generate_pin(length: int) -> Array:
	var pin := []
	pin.resize(length)

	for i in range(length):
		pin[i] = randi() % 10

	return pin


static func filter_dfs(
		node: Node,
		by: FuncRef = funcref(Filter, "always_true")
	) -> void:
	if node.get_child_count() != 0:
		for child in node.get_children():
# warning-ignore:unsafe_cast
			child = child as Node
			if by.call_func(child):
				print(child.get_path())
			filter_dfs(child)


class Filter:
	static func _filter(_node: Node) -> bool:
		return true

	static func always_true(_node: Node) -> bool:
		return true


class NonDefaultCollisionLayerOrMask extends Filter:
	static func _filter(node: Node) -> bool:
		var non_default_collision_layer: bool = \
			node.get("collision_layer") != null and \
			node.get("collision_layer") != 1

		var non_default_collision_mask: bool = \
			node.get("collision_mask") != null and \
			node.get("collision_mask") != 1

		return non_default_collision_layer or non_default_collision_mask

