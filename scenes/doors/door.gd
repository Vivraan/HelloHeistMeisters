extends Area2D

const Character: Script = preload("res://scenes/characters/character.gd")
var _allow_interaction := false
onready var _animation_player := $AnimationPlayer as AnimationPlayer
onready var _player = $"/root".find_node("Player", true, false) as Character


func _on_Door_body_entered(body: Character) -> void:
	if not _player.is_connected("disguise_toggled", self, "_on_disguise_toggled"):
		_player.connect(
			"disguise_toggled", self, "_on_disguise_toggled")
	if body.collision_layer == 1 << HHM_Core.PhysicsLayers2D.PLAYER:
		_allow_interaction = true
	elif body.collision_layer == 1 << HHM_Core.PhysicsLayers2D.NPC:
		_open()
	return


func _on_Door_body_exited(body: Character) -> void:
	if _player.is_connected("disguise_toggled", self, "_on_disguise_toggled"):
		_player.disconnect(
			"disguise_toggled", self, "_on_disguise_toggled")
	if body.collision_layer != 1 << HHM_Core.PhysicsLayers2D.NPC:
		_allow_interaction = false


func _on_disguise_toggled(disguised: bool) -> void:
	_allow_interaction = not disguised


func _on_Door_input_event(_viewport: Node, event: InputEvent, _shape_idx: int) -> void:
	if event.is_action_pressed("interact") and _allow_interaction:
		_open()


func _open() -> void:
	_animation_player.play("Open")
	return
