extends "res://scenes/doors/door.gd"

const Numpad: Script = preload("res://scenes/ui/numpad.gd")
export(int, 4, 8) var _pin_length := 4
onready var _numpad := $CanvasLayer/Numpad as Numpad
onready var _label := $Label as Label


func _ready() -> void:
	_label.rect_rotation = -rotation_degrees
	_numpad.pin = HHM_Core.generate_pin(_pin_length)

	print("%s %s: %s" % [name, get_instance_id(), _numpad.pin])


func _on_Door_body_exited(body: Character) -> void:
	._on_Door_body_exited(body)
	if body.collision_layer != 1 << HHM_Core.PhysicsLayers2D.NPC:
		_numpad.hide()


func _on_Door_input_event(_viewport: Node, event: InputEvent, _shape_idx: int) -> void:
	if event.is_action_pressed("interact") and _allow_interaction:
		_numpad.popup_centered()


func _on_Numpad_entered_correct_pin() -> void:
	_open()
	_numpad.hide()


func _on_Computer_pin_overriden(pin: Array, lockables_group: String) -> void:
	_numpad.pin = pin
	_label.text = lockables_group
