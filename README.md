# HelloHeistMeisters

This project uses addons/plugins not included in the repository - either directly or as submodules. \
Please read [REQUIRES.txt](addons/REQUIRES.txt) to know which addon repositories need to be used, then:

### Windows
1. Get [Link Shell Extension](https://schinagl.priv.at/nt/hardlinkshellext/linkshellextension.html#Download).
1. Use this to make a Symbolic Link Clone between your project's `addons/` folder and the necessary directory (wherever it may be - usually in the `addons/` folder of the plugin repository).

### Any other
Manually create symbolic links to the necessary folder in `addons/`.

You may also directly copy the contents you may need from the addon repositories - however, they're ignored by Git.

**Tip** You can clone the required repos using a script! [REQUIRES.txt](addons/REQUIRES.txt) is written as `name;link` entries separated by new lines (CRLF from Windows, LF from others).
